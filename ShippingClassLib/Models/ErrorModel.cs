﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;

namespace ShippingPortAPI.ShippingClassLib.Models
{
    public enum ErrorType
    {
        validation,
        server,
        unknown,
        auth
    }
    public class Error
    {
        public string field { get; set; }
        public string[] errors { get; set; }
    }
    public class ErrorModel
    {
        public ErrorType type { get; set; } = ErrorType.unknown;
        public string code { get; set; }
        public string message { get; set; }
        public string stack { get; set; }
        public List<Error> errors { get; set; }
        public ErrorModel(ErrorType _type, string _code, string msg, List<Error> errorList = null, string debug = null)
        {
            type = _type;
            code = _code;
            message = msg;
            stack = debug;
            errors = errorList;
        }

        public ErrorModel(ModelStateDictionary modelState)
        {
            List<Error> err = GetErrors(modelState);
            type = ErrorType.validation;
            code = "400";
            errors = err;
            if (err.Count == 1)
            {
                message = err[0].errors[0];
            }

        }
        internal static List<Error> GetErrors(ModelStateDictionary modelState)
        {
            List<Error> errorList = new List<Error>();
            Dictionary<string, string[]> Errors = new Dictionary<string, string[]>();
            foreach (var keyModelStatePair in modelState)
            {
                var key = keyModelStatePair.Key;
                var errors = keyModelStatePair.Value.Errors;
                if (errors != null && errors.Count > 0)
                {
                    if (errors.Count == 1)
                    {
                        var errorMessage = GetErrorMessage(errors[0]);
                        Errors.Add(key, new[] { errorMessage });
                        errorList.Add(new Error { field = key, errors = new[] { errorMessage } });
                    }
                    else
                    {
                        var errorMessages = new string[errors.Count];
                        for (var i = 0; i < errors.Count; i++)
                        {
                            errorMessages[i] = GetErrorMessage(errors[i]);
                        }
                        errorList.Add(new Error { field = key, errors = errorMessages });
                        Errors.Add(key, errorMessages);
                    }
                }
            }
            return errorList;
        }
        internal static List<Error> ConstructErrorMessages(ActionContext context)
        {
            List<Error> errorList = new List<Error>();
            Dictionary<string, string[]> Errors = new Dictionary<string, string[]>();
            foreach (var keyModelStatePair in context.ModelState)
            {
                var key = keyModelStatePair.Key;
                var errors = keyModelStatePair.Value.Errors;
                if (errors != null && errors.Count > 0)
                {
                    if (errors.Count == 1)
                    {
                        var errorMessage = GetErrorMessage(errors[0]);
                        Errors.Add(key, new[] { errorMessage });
                        errorList.Add(new Error { field = key, errors = new[] { errorMessage } });
                    }
                    else
                    {
                        var errorMessages = new string[errors.Count];
                        for (var i = 0; i < errors.Count; i++)
                        {
                            errorMessages[i] = GetErrorMessage(errors[i]);
                        }
                        errorList.Add(new Error { field = key, errors = errorMessages });
                        Errors.Add(key, errorMessages);
                    }
                }
            }
            return errorList;
        }
        private static string GetErrorMessage(ModelError error)
        {
            return string.IsNullOrEmpty(error.ErrorMessage) ?
                "The input was not valid." :
            error.ErrorMessage;
        }
    }
}
