using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using NLog;
using ShippingClassLib.Models;
using ShippingClassLib.Models.DBContext;
using ShippingClassLib.Models.Swagger;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ShippingPortAPI
{
    public class Startup
    {
        private readonly NLog.ILogger _logger;
        private AppConfig appSettings;

        public Startup(IConfiguration configuration)
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
            Configuration = configuration;
            appSettings = new AppConfig();
            Configuration.Bind(appSettings);
            EnvironmentVariableTarget target = EnvironmentVariableTarget.Process;

            Environment.SetEnvironmentVariable("DBConnectionString", appSettings.DBConnectionString, target);

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            

            services.AddControllers();
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.OperationFilter<SwaggerHeaderFilter>();
                c.SwaggerDoc("v2", new OpenApiInfo { Title = "Shipping APP", Version = "V2.0", Description = "<h1>Statutory Warning</h1> <h6>well it's not technically \"statutory\" but the level is same!</h6> Please  note in the error response or in the swagger examples or where ever you find camelcase, please ignore! <br /> All the communication with apis is in <b>snakecase only!!!!</b> <h3>TODO for someone </h3> <blockquote>how to configure swagger to use json prpoerty anotations instead of C# property names?</blockquote>" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme."
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            new string[] {}

                    }
                });

            });
            //services.AddDbContext<ShippingDBContext>(options =>
            //   options.UseSqlServer(connectionStr).EnableSensitiveDataLogging()
            //   );
            services.AddDbContext<ShippingDBContext>(options =>
               options.UseSqlServer(appSettings.DBConnectionString).EnableSensitiveDataLogging()
               );
            services.AddIdentity<ShipModel, IdentityRole<int>>()
                .AddEntityFrameworkStores<ShippingDBContext>()
                .AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(x =>
            {
                x.User.RequireUniqueEmail = false;

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwagger();
            //Enable middleware to serve swagger - ui(HTML, JS, CSS, etc.),
             //specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v2/swagger.json", "Shipping App API V2");
            });
        }

        private Boolean isDBConnected(string connectionString)
        {
            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                //_logger.LogInformation("Connecting to db........... {0}" , connectionString);
                connection.Open();
                connection.Close();
                // _logger.LogInformation("Connecting to db........... {0} successful " , connectionString);
                return true;
            }
            catch (Exception err)
            {
                // use NLog - see example in web api project _logger.LogInformation
                _logger.Error(err, "Error connecting to DB {0}");
                return false;
            }

        }
    }
}
