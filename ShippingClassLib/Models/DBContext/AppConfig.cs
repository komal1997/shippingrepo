﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ShippingClassLib.Models.DBContext
{
    public class AppConfig
    {
        [Required]
        public string DBConnectionString { get; set; }
    }
}
