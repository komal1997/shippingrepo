﻿using Microsoft.Extensions.Logging;
using ShippingClassLib.Models;
using ShippingClassLib.Models.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShippingClassLib.DAL
{
    public class ShipDAL
    {
        private readonly ILogger _logger;

        public ShipDAL(ILogger logger)
        {
            _logger = logger;
        }

        #region Get all ship data
        /// <summary>
        ///  Get all ship data
        /// </summary>
        /// <param name="context">context</param>
        /// <returns></returns>
        public IEnumerable<ShipModel> GetAllShipData(ShippingDBContext context)
        {
           
            return context.Users.Select(u => new ShipModel()
            {
                ShipName = u.ShipName,
                ShipVelocity = u.ShipVelocity,
                CurrentLatitude = u.CurrentLatitude,
                CurrentLongitude = u.CurrentLongitude,
                
            });
        }
        #endregion


        #region Add New Ship Data
        /// <summary>
        ///  Add New Ship Data
        /// </summary>
        /// <param name="context">context</param>
        /// <returns></returns>
        public void AddShipData (ShippingDBContext context,ShipModel oData)
        {
            context.Add(oData);
            context.SaveChanges();
            _logger.LogInformation("New ShipID : " + oData.ShipID);
        }
        #endregion
    }
}
