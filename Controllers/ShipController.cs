﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShippingClassLib.DAL;
using ShippingClassLib.Models;
using ShippingClassLib.Models.DBContext;
using ShippingPortAPI.ShippingClassLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShippingPortAPI.Controllers
{
    [Route(CustomUtility.RouteUrl)]
    public class ShipController : Controller
    {
        private UserManager<ShipModel> userManager;
        private readonly ILogger<ShipController> logger;
        private readonly ShippingDBContext context;

        public ShipController(UserManager<ShipModel> userManager, ILogger<ShipController> _logger,
                                ShippingDBContext _context)
        {
            this.userManager = userManager;
            this.logger = _logger;
            this.context = _context;
        }

        #region Get All Ship Data
        /// <summary>
        /// Get All Ship Data
        /// </summary>
        /// <returns>Ship data</returns>
        [HttpGet]
        [Route("GetAllShipData")]
        public IActionResult GetAllShipData()
        {

            List<ShipModel> user = new ShipDAL(logger).GetAllShipData(context).ToList();
            return Ok(user);
        }
        #endregion

        #region Add New Ship Data
        /// <summary>
        /// Add Ship Data
        /// </summary>
        /// <returns>Ship Id</returns>
        [HttpPost]
        public async Task<IActionResult> AddShipData([FromBody] ShipModel oShipData)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Check if Same ship data exists
                    if (context.ShipData.Any(o => o.ShipName == oShipData.ShipName && o.CurrentLatitude == oShipData.CurrentLatitude && o.CurrentLongitude == oShipData.CurrentLongitude))
                    {
                        return BadRequest(new ErrorModel(_type:ErrorType.validation,_code:"409",msg:"Ship with same name and location allready exists.")) ;
                    }
                    new ShipDAL(logger).AddShipData(context, oShipData);

                    return StatusCode(204);
                }
                catch (Exception ex)
                {
                    logger.LogError("AddShipData :" + ex.Message);
                    return StatusCode(500);
                }
            }
            else
            {
                return BadRequest(new ErrorModel(ModelState));
            }
        }
        #endregion
    }
}
