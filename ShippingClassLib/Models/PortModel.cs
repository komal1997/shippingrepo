﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ShippingClassLib.Models
{
    [Table("portmaster")]
    public class PortModel 
    {
        [Column("PortID")]
        public int PortID { get; set; }

        [Column("PortName")]
        public string PortName { get; set; }

        [Column("Code")]
        public string Code { get; set; }

        [Column("PortType")]
        public string PortType { get; set; }

        [Column("PortSize")]
        public string PortSize { get; set; }

        [Column("Status")]
        public string Status { get; set; }

        [Column("MaxDepth")]
        public string MaxDepth { get; set; }

        [Column("MaxLength")]
        public string MaxLength { get; set; }

        [Column("annualcapacitymt")]
        public string AnnualCapacitymt { get; set; }

        [Column("locprecision")]
        public string LocPrecision { get; set; }

        [Column("latitude")]
        public double Latitude { get; set; }

        [Column("longitude")]
        public double Longitude { get; set; }

        [Column("iso3")]
        public string Iso3 { get; set; }

        [Column("iso3_op")]
        public string Iso3Op { get; set; }

        [Column("country")]
        public string Country { get; set; }

    }
}
