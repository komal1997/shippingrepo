﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace ShippingPortAPI.DAL.DBManagers
{
    public interface IDBManager
    {
        /// <summary>
        /// Always call Dispose object after compleiting all DB related task.
        /// </summary>
        void Dispose();

        /// <summary>
        /// Prepare Stored Procedure Command object. By calling method DbCommand will re-init with new value.
        /// </summary>
        ///<param name="cmdtext">Used to set sp name.</param>
        void PrepareStoredProcedureCommand(string cmdtext);


        /// <summary>
        /// Prepare Query Text DbCommand object. By calling method DbCommand will re-init with new value.
        /// </summary>
        /// <param name="cmdtext">Used to set query text.</param>
        void PrepareTextCommand(string cmdtext);


        /// <summary>
        /// Add Parameter in DbCommand. Prepare DbCommand before using this method.
        /// </summary>
        /// <param name="parametername">Used to set parameter name.</param>
        /// <param name="value">Used to set parameter value.</param>
        void AddParameter(string parametername, object value);


        /// <summary>
        /// Add Parameter in DbCommand. Prepare DbCommand before using this method.
        /// </summary>
        /// <param name="parametername">Used to set parameter name.</param>
        /// <param name="value">Used to set parameter value.</param>
        /// <param name="dbtype">Used to set parameter DbType.</param>
        void AddParameter(string parametername, object value, DbType dbtype);


        /// <summary>
        /// Add Out Parameter in DbCommand. Prepare DbCommand before using this method.
        /// </summary>
        /// <param name="parametername">Used to set parameter name.</param>
        /// <param name="dbtype">Used to set parameter DbType.</param>
        void AddOutParameter(string parametername, DbType dbtype);

        /// <summary>
        /// Get Out tParameter. Prepare DbCommand before using this method.
        /// </summary>
        /// <param name="parametername">Used to get value based on parameter name.</param>
        T GetOutParameter<T>(string parametername);

        /// <summary>
        /// Execute Non Query. Always execute DbCommand before using this method.
        /// </summary>
        /// <returns>Returns the number of rows affected.</returns>
        Task<int> ExecuteNonQueryAsync();

        /// <summary>
        /// Execute Non QueryAsync. Always execute DbCommand before using this method.
        /// </summary>
        /// <returns>Returns A System.Data.Common.DbDataReader object.</returns>
        //Task<DbDataReader> ExecuteReaderAsync();

        Task<List<DataTable>> ExecuteCommandAsync();

        DataSet ExecuteDataSet();
    }
}
