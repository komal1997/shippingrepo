﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShippingClassLib.Models.DBContext
{
    public class ShippingDBContext : IdentityDbContext<ShipModel, IdentityRole<int>, int>
    {
        public DbSet<PortModel> PortData { get; set; }
        public DbSet<ShipModel> ShipData { get; set; }
        public ShippingDBContext(DbContextOptions<ShippingDBContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ShipModel>(b =>
            {
                b.ToTable("ShipMaster");
            });
            builder.Entity<PortModel>(b =>
            {
                b.HasNoKey();
                b.ToTable("portmaster");
            });
            
        }
    }
}
