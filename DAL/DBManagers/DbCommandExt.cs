﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace ShippingPortAPI.DAL.DBManagers
{
    internal static class DbCommandExt
    {
        public static SqlCommand CreateDbCMD(this SqlConnection con, CommandType cmdtype, string cmdtext)
        {
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = cmdtype;
            cmd.CommandText = cmdtext;
            cmd.Connection = con;
            return cmd;
        }

        public static SqlCommand CreateStoredProcedureDbCMD(this SqlConnection con, string cmdtext)
        {
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = cmdtext;
            cmd.Connection = con;
            return cmd;
        }

        public static void AddCMDParam(this SqlCommand cmd, string parametername, object value)
        {
            DbParameter param = cmd.CreateParameter();
            param.ParameterName = parametername;
            param.Value = value;
            cmd.Parameters.Add(param);
        }
        public static void AddCMDParam(this SqlCommand cmd, string parametername, object value, DbType dbtype)
        {
            DbParameter param = cmd.CreateParameter();
            param.ParameterName = parametername;
            param.Value = value != null ? value : DBNull.Value;
            param.DbType = dbtype;
            cmd.Parameters.Add(param);
        }

        public static IDataParameter AddCMDOutParam(this SqlCommand cmd, string parametername, DbType dbtype)
        {
            DbParameter param = cmd.CreateParameter();
            param.ParameterName = parametername;
            param.DbType = dbtype;
            param.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param);
            return param;
        }

        public static bool IsDBNull(this DbDataReader dataReader, string columnName)
        {
            return dataReader[columnName] == DBNull.Value;
        }

        public static DataSet ExecuteDataSet(this SqlCommand cmd)
        {
            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDataAdapter = new SqlDataAdapter();
            cmd.CommandTimeout = 0;
            oDataAdapter.SelectCommand = cmd;
            oDataAdapter.Fill(oDataSet);
            return oDataSet;
        }

        public static DataTable ExecuteDataTable(this SqlCommand cmd)
        {
            DataTable oDataTable = new DataTable();
            SqlDataAdapter oDataAdapter = new SqlDataAdapter();
            cmd.CommandTimeout = 0;
            oDataAdapter.SelectCommand = cmd;
            oDataAdapter.Fill(oDataTable);
            return oDataTable;
        }

        public static Int32 ToInt32(this DataRow dtRow, string columnName)
        {
            return dtRow.IsNull(columnName) ? 0 : Convert.ToInt32(dtRow[columnName]);
        }


        public static long ToInt64(this DataRow dtRow, string columnName)
        {
            return dtRow.IsNull(columnName) ? 0 : Convert.ToInt64(dtRow[columnName]);
        }


        public static bool HasRows(this DataTable dtable)
        {
            return dtable.Rows.Count > 0;
        }
    }

}
