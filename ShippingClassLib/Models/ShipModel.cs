﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ShippingClassLib.Models
{
    public class ShipModel : IdentityUser<int>
    {
        [Column("ShipID")]
        public int ShipID { get; set; }
        [Column("ShipName")]
        public string ShipName { get; set; }

        [Column("ShipVelocity")]
        public decimal? ShipVelocity { get; set; }

        [Column("CurrentLatitude")]
        public decimal? CurrentLatitude { get; set; }

        [Column("CurrentLongitude")]
        public decimal? CurrentLongitude { get; set; }

    }
}
