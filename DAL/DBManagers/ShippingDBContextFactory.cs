﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Werq_Space.DAL.DBManagers
{
    public class ShippingDBContextFactory
    {
        public static SqlConnection GetShippingDBConnection(string connectionString = null)
        {
            if (null == connectionString)
                return new SqlConnection(Environment.GetEnvironmentVariable("DBConnectionString"));
            else
                return new SqlConnection(connectionString);
        }

    }
}
