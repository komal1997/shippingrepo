using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShippingPortAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

            try
            {
                string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                if (null == env)
                {
                    logger.Error("!!!!!!!!!!! ASPNETCORE_ENVIRONMENT NOT SET .........");
                }
                else
                {
                    logger.Debug("init main....... ASPNETCORE_ENVIRONMENT {0}", env);
                    CreateHostBuilder(args).Build().Run();
                }

            }
            catch (Exception ex)
            {
                //NLog: catch setup errors
                logger.Error(ex, "Stopped program because of exception.........");
                Environment.Exit(1);
                throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                NLog.LogManager.Shutdown();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    IHostEnvironment env = hostingContext.HostingEnvironment;
                    config.AddJsonFile("hosting.json", optional: false, reloadOnChange: true);
                    config.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true);
                    config.AddJsonFile($"certificate.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                    config.SetBasePath(env.ContentRootPath);
                    config.AddEnvironmentVariables();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel((hostingContext, serverOptions) =>
                    {
                        IHostEnvironment env = hostingContext.HostingEnvironment;
                        serverOptions.Limits.MaxConcurrentConnections = 100;
                        serverOptions.Limits.MaxConcurrentUpgradedConnections = 100;
                        serverOptions.Limits.MaxRequestBodySize = 10 * 1024;
                        serverOptions.Limits.MinRequestBodyDataRate = new MinDataRate(bytesPerSecond: 100, gracePeriod: TimeSpan.FromSeconds(10));
                        serverOptions.Limits.MinResponseDataRate = new MinDataRate(bytesPerSecond: 100, gracePeriod: TimeSpan.FromSeconds(10));

                        string apiport = hostingContext.Configuration.GetSection(env.EnvironmentName).GetValue<String>("port");
                        if (env.EnvironmentName == "Development")
                        {
                            serverOptions.ListenAnyIP(Convert.ToInt32(apiport));
                        }
                        else
                        {
                            string certificateFileName = hostingContext.Configuration["certificateSettings:filename"].ToString();
                            string certificatePassword = hostingContext.Configuration["certificateSettings:password"].ToString();
                            serverOptions.ListenAnyIP(Convert.ToInt32(apiport),
                               listenOptions =>
                               {
                                   listenOptions.UseHttps(certificateFileName, certificatePassword);
                               });
                        }
                    });
                    webBuilder.UseStartup<Startup>().CaptureStartupErrors(true);
                })
                .UseNLog();

    }
}
