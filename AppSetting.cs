﻿using Microsoft.Extensions.Configuration;
using System;

namespace ShippingPortAPI
{
    public class AppSetting
    {

        public AppSetting(IConfiguration Configuration)
        {
            //test
            EnvironmentVariableTarget target = EnvironmentVariableTarget.Process;

            string ConStr = Configuration["DBConnectionString"];
            Environment.SetEnvironmentVariable("DBConnectionString", ConStr, target);

        }
    }
}
